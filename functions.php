<?php
    function begin_session() {
        session_start();
    }
    function end_session() {
        $_SESSION = array();
        if (isset ($_COOKIE[session_name])) {
            setcookie(session_name(), '', time()-42000, '/');
        }
        session_destroy();
    }

function show_register(){	
	global $myurl;
	include("view/register.html");
}

function show_index(){	
	global $myurl;
	include("view/index.html");
}

function show_upload(){	
	global $myurl;
	include("view/upload.html");
}

function show_about(){	
	global $myurl;
	include("view/about.html");
}

function show_random($moments){	
	global $myurl;
    $random=array_rand($moments,1);
	include("view/random.html");
}

function show_login(){
		global $myurl;
		include("view/login.html");
	}	

function authenticate(){
	global $myurl;
	$errors=array();
	$username="";
	$password="";
	if (isset($_POST['username']) && $_POST['username']!="") {
		$username=$_POST['username'];
	} else {
		$errors[]="No username detected!";
	}
	if (isset($_POST['password']) && $_POST['password']!="") {
		$password=$_POST['password'];
	} else {
		$errors[]="Please enter password!";	
	}
	
	if (empty($errors)){
		if ($username=="usr" && $password=="pswd"){
			$_SESSION['username']=$username;
			$_SESSION['user_id']=0;
			$_SESSION['notice'][]="Hi ".htmlspecialchars($username).", welcome back!";
			header("Location: $myurl");
			
		} else {
			$errors[]="Username and password do not match.";
            $_SESSION['username']=$_POST['username'];	
			include("view/login.html");
		}	
	} else {
		include("view/login.html");
	}
}

function logout() {
    global $myurl;
        end_session();
        header("Location: $myurl");
}
?>