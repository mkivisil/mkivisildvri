<?php
require_once("functions.php");
$myurl=$_SERVER['PHP_SELF'];
begin_session();

$moments=array(
            array("img"=>"uploaded/wowrandom.jpg", "title"=>"WoW Glitch", "alt"=>"wowglitch"),
            array("img"=>"uploaded/someglitch.jpg", "title"=>"Some Glitch", "alt"=>"someglitch")
        );

$mode="index";

if(isset($_GET['mode']) && $_GET['mode']!="") {
    $mode=$_GET['mode'];
}

include_once("view/head.html");

switch($mode) {
    case "register":
        show_register();
    break;
    case "upload":
        show_upload();
    break;
    case "login":
        show_login();
    break;
    case "random":
        show_random($moments);
    break;
    case "about":
        show_about();
    break;
    case "auth":
        authenticate();
    break;
    case "upl":
        upload();
    break;
    case "logout":
        logout();
    default: show_index();
}

include_once("view/foot.html");
?>
